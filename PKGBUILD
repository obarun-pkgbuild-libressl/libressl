# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://aur.archlinux.org/packages/libressl/	
## Maintainer : Levente Polyak <anthraxx[at]archlinux[dot]org>
## Contributor: Reventlov <contact@volcanis.me>
#--------------------------------------------------------------------------------------

pkgname=libressl
pkgver=2.9.1
pkgrel=1
arch=('x86_64')
license=('custom:OpenSSL')
_website="http://www.libressl.org/"
pkgdesc="LibreSSL is a version of the TLS/crypto stack forked from OpenSSL in 2014"
url="http://ftp.openbsd.org/pub/OpenBSD/LibreSSL"
source=("$url/$pkgname-$pkgver.tar.gz"{,.asc}
    'fail-instead-of-trying-fallback.patch')

conflicts=(
    'openssl'
    'openssl-1.0')

backup=(
    'etc/ssl/openssl.cnf')

depends=(
    'glibc')

optdepends=(
    'ca-certificates')

#--------------------------------------------------------------------------------------
prepare() {
    cd "$pkgname-$pkgver"

    ## maximum security
    patch -Np1 -i ../fail-instead-of-trying-fallback.patch

    ## fix manpage symlink locations
    sed -ri 's|(ln -sf )(.+) (.+)|\1\2.gz \3.gz|g' man/Makefile.in
}

build() {
    cd "$pkgname-$pkgver"

    ./configure                     \
        --prefix=/usr               \
        --with-openssldir=/etc/ssl
    make
}

check() {
    cd "$pkgname-$pkgver"

    make check
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir" install

    install -Dm 644 COPYING "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

    rm "$pkgdir/etc/ssl/cert.pem"
    #ln -s /usr/lib/libssl.so "$pkgdir/usr/lib/libssl.so.1.0.0"
    #ln -s /usr/lib/libcrypto.so "$pkgdir/usr/lib/libcrypto.so.1.0.0"
    #ln -s /usr/lib/libtls.so "$pkgdir/usr/lib/libtls.so.1.0.0"
}

#--------------------------------------------------------------------------------------
validpgpkeys=('A1EB079B8D3EB92B4EBD3139663AF51BD5E4D8D5' # Brent Cook <bcook@openbsd.org>
)
sha512sums=('7051911e566bb093c48a70da72c9981b870e3bf49a167ba6c934eece873084cc41221fbe3cd0c8baba268d0484070df7164e4b937854e716337540a87c214354'
            'SKIP'
            'e93be3e904e06c3fbcc249a53dbb56e818364c5022b6a2fe7e141a0f0b7915b2ff7c02a22ce37f1daf536592f3b1ac16d8fea17b133a2bfc7a4d2891bef4ab18')
